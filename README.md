# alta3research-ansible-cert

A project for submission to Alta3 as the basis for Ansible Certification as part of their Ansible Essentials Course

# What does it do?
It queries data about Astronauts in space from "http://api.open-notify.org/astros.json" and displays the current number under the moniker 
"Space Aces".

Simply clone this repository and run **ansible-playbook alta3research-ansiblecert01.yml**

## Authors and acknowledgment
Éanna O'Sullivan eanna.osullivan@dell.com

## License
GNU General Public License v3.0 - see license details in the project for more information.

